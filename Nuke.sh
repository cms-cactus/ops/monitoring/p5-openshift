#!/bin/env sh

printf "\n"

oc project

printf "Check the above message to verify this is the project you want to delete.\n"

read -p "THIS ACTION IS IRREVERSIBLE. Are you want to delete EVERYTHING in the selected project? " -n 1 -r
printf "\n"
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  printf "Operation aborted. Nothing has been deleted.\n"
  exit 1
fi

oc delete --all imagestreams
oc delete --all secrets
oc delete --all configmaps
oc delete --all dc
oc delete --all services
oc delete --all pods
oc delete --all routes