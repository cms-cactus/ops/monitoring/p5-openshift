## Utility script for deployments to openshift

services=(l1ce \
  l1ce-test \
  l1page \
  l1page-test \
  l1ts-bmtf \
  l1ts-calol1 \
  l1ts-calol2 \
  l1ts-central \
  l1ts-cppf \
  l1ts-emtf \
  l1ts-omtf \
  l1ts-twinmux \
  l1ts-ugmt \
  l1ts-ugt \
  l1ts-xaas   \
  cmsrc-trigger)


## CREATE

for service in ${services[@]}; do
  oc process curl-mirror \
    -p HTTP_PROXY=socks5h://ssh-proxy:55555 \
    -p APP_NAME=${service} | \
    oc create -f -
done