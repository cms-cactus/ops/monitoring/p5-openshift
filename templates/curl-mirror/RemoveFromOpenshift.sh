## Utility script to delete all services and deployment configs related to curl-mirror

services=(l1ce \
  l1ce-test \
  l1page \
  l1page-test \
  l1ts-bmtf \
  l1ts-calol1 \
  l1ts-calol2 \
  l1ts-central \
  l1ts-cppf \
  l1ts-emtf \
  l1ts-omtf \
  l1ts-twinmux \
  l1ts-ugmt \
  l1ts-ugt \
  l1ts-xaas   \
  cmsrc-trigger)

## DELETE

for service in ${services[@]}; do 
  oc delete service/${service}
  oc delete deploymentconfig/${service}
done