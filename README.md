# OpenShift P5 infrastructure

OpenShift configuration replicating the P5 monitoring infrastructure at P5.\
Prerequisites:

* CERN computing account
* CMS Point-5 account
* Monitoring DB password

## Setup

### Create and setup a CERN OpenShift project

First, create an OpenShift application.

To do so:

1) Go on [CERN Web Services](https://webservices.web.cern.ch/webservices/)
2) Select "Create a new website"
3) Select "Test" as site category, choose a name, and **make sure you select PaaS Web Application** as site type.

Agree to CERN computing rules and create it.
The web site will be available after around 20 minutes, you will receive an email once it is ready.
Once ready, make sure you increase the resource quota to large, or the project will not fit.
To do that:

1) Go on [CERN Web Services](https://webservices.web.cern.ch/webservices/)
2) Select "Manage my websites"
3) Select your project
4) Select "View quota usage"
5) Choose Large in Choose the flavor

### Building the infrastructure

#### Creating the prometheus DB storage

Go to CERN OpenShift at [https://openshift-dev.cern.ch](https://openshift-dev.cern.ch).
Select your project in the right panel, then select the Storage section in the left panel in the OKD interface.
Select **Create Storage**, choose **cephfs** as storage class, name the storage **prometheus-storage**, select Shared Access (RWX) as access mode.
As size 10 GiB will be enough and provide storage space for 20 days.
Press **Create**.

### Adding the services

In this repository's folder, start a Docker container loaded with the OpenShift CLI tools with:

```bash
docker run -it --mount type=bind,source=$(pwd),target=/p5-openshift  gitlab-registry.cern.ch/paas-tools/openshift-client /bin/bash
```

The ```--mount``` option mounts the repository's folder at ```/p5-openshift``` in the Docker container.
In the container connect to CERN OpenShift with ```oc login https://openshift-dev.cern.ch``` and insert your CERN username and password when required.

If this is your first and only OpenShift project, then OpenShift will automatically select the project you previously created for you. Else, type ```oc project <PROJECT_NAME>``` to select the project you created before.

Finally, run ```./Deploy.sh``` to deploy the monitoring project in OpenShift.
You will be required to input your P5 username and password, and the monitoring DB password.
If everything goes smoothly, you will have your infrastructure ready in a couple of minutes.
Routes connecting services to the external network are also pre-installed:

* Prometheus can be accessed at [http://prometheus-<PROJECT_NAME>.web.cern.ch](http://prometheus-<PROJECT_NAME>.web.cern.ch).
* Prometheus can be accessed at [http://grafana-<PROJECT_NAME>.apptest.cern.ch](http://grafana-<PROJECT_NAME>.apptest.cern.ch).
* Prometheus can be accessed at [http://alertmanager-<PROJECT_NAME>.apptest.cern.ch](http://alertmanager-<PROJECT_NAME>.apptest.cern.ch).

### Deleting everything

If anything goes wrong, you can delete everything you created by running ```./Nuke.sh``` and try to rebuild.\
Beware, rebuilding too many times might bother docker.io (which is the repository used by alertmanager), which might stop checkouts for a while and cause further failures.
You can check if this is happening by going into your project management at [https://openshift-dev.cern.ch](https://openshift-dev.cern.ch), selecting your project and checking the alertmanager ImageStream under ```Builds > Images```.

## Components

The cluster consists of three main deployments:

* Prometheus, which has all the monitoring tools and exporters and is located in ```deployment_configs/prometheus.yaml```
* SSH tunnel featuring a SOCKS5 proxy server to P5 network, located in ```deployment_configs/ssh-proxy.yaml```
* cURL proxying HTTP requests to P5 hosts, based on the template at ```templates/curl-mirror/curl-mirror.template.yml```

### Prometheus

The Prometheus deployment contains:

* Prometheus DB, for monitoring
* sql-exporter, for exporting monitoring DB data
* l1t-xaas-exporter, to export flashlists
* alertmanager, for alerting
* blackbox-exporter, to check status of services
* grafana, for visualising monitoring info

HTTP requests from l1t-xaas-exporter and blackbox-exporter can not be sent to P5 hosts because they are only accessible with a SOCKS5 proxy, which is not supported.
Therefore, these requests are actually sent to pods with the same name in the local OpenShift cluster.
These pods are loaded with a HTTP server listening to the requests and forwarding them with cURL, which supports SOCKS5 proxies.

### SSH tunnel

These deployment deploys a pod with a simple SSH connection to cmsusr and dynamically forwarding connections to port 55555 to P5 hosts with a SOCKS5 proxy.

### cURL proxies

These are created from a base template at ```templates/curl-mirror/curl-mirror.template.yml```.
Each one of these has a simple HTTP server listening to requests on specific paths, e.g. ```/metrics```, and ports, corresponding to the same paths and ports P5 machines are supposed to reply. When a request is received on that path and port, a cURL command is run to contact a specific P5 machine on the same path and port.
Each P5 machine has a corresponding cURL proxy.
