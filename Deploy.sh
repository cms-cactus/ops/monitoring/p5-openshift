#!/bin/env sh

# Configuration

# hosts in p5 that must be accessed
p5hosts=(l1ce \
  l1ce-test \
  l1page \
  l1page-test \
  l1ts-bmtf \
  l1ts-calol1 \
  l1ts-calol2 \
  l1ts-central \
  l1ts-cppf \
  l1ts-emtf \
  l1ts-omtf \
  l1ts-twinmux \
  l1ts-ugmt \
  l1ts-ugt \
  l1ts-xaas   \
  cmsrc-trigger \
  scoutdaq-s1d12-39-01)

set +o allexport  # make sure variables are not automatically exported
shopt -s nullglob

set -o errexit # exit when any command fails
# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG

set -o nounset # exit on unset variable
# echo an error message before exiting
trap 'echo "\"${last_command}\" command failed with exit code $?."' EXIT

unset -v P5_PASS # make sure it's not exported
unset -v MON_DB_PASS 
unset -v tmpP5psw
unset -v tmpMONDBpsw

# Building

oc project

read -p "Building P5 monitoring infrastructure in this project. Are you sure?" -n 1 -r
printf "\n"
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
  printf "Operation aborted. Nothing has been changed.\n"
  exit 1
fi

PROJECT_NAME=$(oc project -q)

printf "Insert P5 username:\n"
read P5_USERNAME < /dev/tty

printf "Insert P5 password:\n"
read -rs P5_PASS < /dev/tty

printf "Insert same password again:\n"
read -rs tmpP5psw < /dev/tty

if [[ $P5_PASS != $tmpP5psw ]]; then
  echo "P5 passwords do not match"
  exit 1
fi

printf "Insert monitoring DB password (user cms_trg_r):\n"
read -rs MON_DB_PASS < /dev/tty

printf "Insert same password again:\n"
read -rs tmpMONDBpsw < /dev/tty

if [[ "$MON_DB_PASS" != "$tmpMONDBpsw" ]]; then
  printf "Monitoring DB passwords do not match\n"
  exit 1
fi

printf "Saving output at log_deploy.log\n"


printf "Importing imagestreams\n"
# Importing imagestreams
for f in imagestreams/*{.yaml,.yml}; 
do 
  # image process
  oc create -f $f >> log_deploy.log 2>>log_deploy.log
done

printf "Importing secrets and config maps\n"
# Importing Secrets and config maps
kubectl create secret generic cms-monitoring-db-password --from-literal=cms_trg_r=${MON_DB_PASS} >> log_deploy.log 2>>log_deploy.log
kubectl create secret generic p5-password --from-literal=password=${P5_PASS} >> log_deploy.log 2>>log_deploy.log
kubectl create configmap p5-username --from-literal=username=${P5_USERNAME} >> log_deploy.log 2>>log_deploy.log

for f in configs/*{.yaml,.yml}; 
do 
  oc create -f $f >> log_deploy.log 2>>log_deploy.log
done

printf "Importing deployment configs\n"
# Importing deployment configs 
# proxies for individual P5 services are generated via curl-mirror templates
for f in deployment_configs/*{.yaml,.yml}; 
do 
  oc create -f $f >> log_deploy.log 2>>log_deploy.log
done

printf "Creating P5 proxies\n"
# Creating p5 proxies
for service in ${p5hosts[@]}; do
  oc process \
    -p HTTP_PROXY=socks5h://ssh-proxy:55555 \
    -p APP_NAME=${service} -f templates/curl-mirror/curl-mirror.template.yml | \
    oc create -f - >> log_deploy.log 2>>log_deploy.log
done


printf "Importing services\n"
# Importing services
for f in services/*{.yaml,.yml}; do
  oc create -f $f >> log_deploy.log 2>>log_deploy.log
done

printf "Exposing prometheus\n"
oc expose svc/prometheus --hostname prometheus-${PROJECT_NAME}.apptest.cern.ch 
printf "Exposing grafana\n"
oc expose svc/grafana --hostname grafana-${PROJECT_NAME}.apptest.cern.ch 
printf "Exposing alertmanager\n"
oc expose svc/alertmanager --hostname alertmanager-${PROJECT_NAME}.apptest.cern.ch 

printf "Done!\n"

trap - EXIT
trap - DEBUG